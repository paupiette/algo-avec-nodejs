

const data = require('./data.js'); //objet récupéré de data.js --> entre parenthèses le nom du fichier qui contient module.exports sans le point js
//console.log(data);



//1.TRIER PAR GENRE

//faire tab avec tous les hommes, et un avec toutes les femmes
var homme = [];
var femme = [];
//regarder gender 

var tous = data.results; //tableau puis objt

//console.log(tous[0].gender)

//si gender = female mettre dans le tableau des femmes
//si gender = male mettre dans le tableau des hommes

for(var i = 0; i<tous.length; i++){

    if(tous[i].gender == 'male'){

        homme.push(tous[i]);

    }
    if(tous[i].gender == 'female'){

        femme.push(tous[i]);

    }
}

//afficher le pourcentage de femme
var pf = femme.length / tous.length * 100;
//console.log(pf);







//2.TRIER POUR TROUVER LE VIEUX
//homme le plus vieux
//femme la plus vieille
//personne la plus vieille
//fonction plus_vieux(tab) -> 1personne
//afficher le nom, le prénom et l'âge du plus vieux

//là c'est le code de maxime

function plus_vieux(tab){

    var le_plus_vieux = tab[0]

    for(let personne of tab){
        if(le_plus_vieux.dob.age < personne.dob.age){
            le_plus_vieux = personne
        }
    }

    return le_plus_vieux
}

function afficherPersonne(p){
    console.log(`${p.name.title} ${p.name.first} ${p.name.last} : ${p.dob.age} ans`)
}

//l'homme le plus vieux
var homme_vieux = plus_vieux(homme);
afficherPersonne(homme_vieux)

//la femme la plus vieille
var femme_vieille = plus_vieux(femme);
afficherPersonne(femme_vieille)

//personne la plus vieille
var personne_vieille = plus_vieux(tous);
afficherPersonne(personne_vieille)