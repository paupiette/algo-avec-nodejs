# algo avec nodejs

## Dans ton terminal /péparation/:

<pre><code>
mkdir algo
cd algo
touch algo.js
touch data.js
</code></pre>

après avoir créé ton doss et tes fichiers vérifie si tu as node 
<pre><code>node</code></pre>

si il est pas installé tu fais juste la commande <pre><code>apt get install nodejs</code></pre> qu'il te propose

et ensuite pour voir ce que fais ton code il te suiffira d'écrire : <pre><code>node algo.js</code></pre> dans ton terminal


## Dans ton navigateur /péparation/:

https://randomuser.me/api/?inc=name,dob,gender&results=100

randomuser est une api qui te permet de créer des faux profil de gens qui n'existent pas et de récupérer leurs données au format JSON

à partir de '?inc=name,dob,gender' => tu demandes de récupérer que ce type de données des faux profils créés '=100' ça veut dire que tu veux 100 faux profils.

1. tu copies colles l'adresse du dessus dans ton navigateur, ça va t'afficher un fichier au format JSON, 

2. tu cliques sur "Données brutes" en haut de l'écran tu copies colles les objets et tu les mets dans ce fichier, 

3. maxime nous les a fait mettre dans la constante data

4. après ta constante : module.exports = data;

5. Tout le reste va se passer dans le fichier algo.js --> là ce sont les algo qu'on a fait